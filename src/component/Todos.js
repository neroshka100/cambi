import { useState } from "react";
import React, { Component } from 'react';
import Todo from "./Todo";
import "./Todos.css";
import InputUnstyled from '@mui/base/InputUnstyled';
import { styled } from '@mui/system';

let newTodo= ""; 

function Todos(){
    const [todos, setTodos]= useState([
         {
                id: 1,
                title: "Wash the house",
                completed: false,
            },
            {
                id: 2,
                title: "Sumbit work",
                completed: false,
            },
            {
                id: 3,
                title: "Sign up for an English course",
                completed: true,
            },
            {
                id: 4,
                title: "Learn to React",
                completed: false,
            },
           
        ]);
           
const removeTodo = (id) =>{
    setTodos(todos.filter((todo)=>todo.id!==id));
}
const toggleTodo = (id) => {
    setTodos(
        todos.map((todo) =>
             todo.id==id ?{ ...todo, completed: ! todo.completed} : todo ))
}
        
    return(
       <div >
           <div className="input"> 
            <input type="text" onChange={(event) => (newTodo = event.target.value)} />
             <button
            onClick={()=>{
                setTodos([
                     ...todos,
                    {
                        id:todos.length+1,
                        title: newTodo,
                        completed: false,
                    },
                   
                ]);
            }}
            > 
            Add
            </button></div> 
            
          {todos.map(({id, completed, title})=>(
          <div key={id}>
              <Todo id={id} completed={completed} onRemove={removeTodo} onToggle={toggleTodo}>{title} </Todo> 
             </div>
         ))} 
       </div> 
    )
    
}
//     let newTodo= " "; 

// const [todos, setTodos]= useState([
//     {
//         id: 1,
//         title: "wash",
//         completed: false,
//     },
//     {
//         id: 2,
//         title: "throw",
//         completed: true,
//     },
    
// ]);

// useEffect(()=>{
//     console.log("use effect called");
//     document.title= `Todos: ${todos.length}`;
// },[todos]);
// const removeTodo = (id)=>{
//     setTodos (todos.filter((todo)=>todo.id!=id));
// };
// const toggleTodo =(id)=>{
//     setTodos(
//         todos.map((todo)=>
//         todo.id=id?{...todo, completed:!todo.completed}:todo)
//     );
// };

//     return(
//         <div>
//             <div> Todos count: {todos.length}</div> 
//             <input> type="text" onChenge={(e)=>(newTodo=e.target.value)}</input>
//             <button 
//             onClick={()=>{
//                 setTodos([
//                     {
//                         id:todos.length+1,
//                         title: newTodo,
//                         completed: false,
//                     },
//                     ...todos,
//                 ]);
//             }}
//             > Add new todo
//             </button>
//             {todos.map(({id, completed, title})=>(
//                 <h1 key={todos.id}>
//                     <Todo
//                     id={id}
//                     completed={completed}
//                     onRemove={removeTodo}
//                     onToggle= {toggleTodo}
//                     >
//                         {title}
//                         </Todo>
//                 </h1>

//         ))}
//         </div>
        
//     );
    
    
//  }
export default Todos;