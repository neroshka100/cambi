import "./Todo.css"
import { AccessAlarm, ThreeDRotation } from '@mui/icons-material';
import DeleteIcon from '@mui/icons-material/Delete';


function Todo({id, title, completed, children, onRemove, onToggle}){
    return(
    
     <div className="title">
     <span onClick={() => onToggle(id)} style={completed ? {textDecoration: "line-through"}:{}}> 
       {children}
     </span>

     <button onClick={()=> onRemove(id)}
      className="delete">Delete</button>
      </div>
 
     

    );
}
export default Todo;